-- CREATE OR REPLACE FUNCTION search_jobs(limitVal INT, offsetVal INT, keyword TEXT, product_category_codes TEXT[])
-- RETURNS SETOF custom_result_search_jobs
-- AS $$
-- DECLARE
--     user_id uuid;
--     session_variables json;
-- BEGIN
--     session_variables := current_setting('hasura.user', 't');
--     user_id := (session_variables->>'x-hasura-user-id')::uuid;
--     return query select * from (
--       values (user_id),
--              (user_id)
--     ) as x(tranche_id);
-- END;
-- $$ LANGUAGE plpgsql;

-- CREATE EXTENSION IF NOT EXISTS pg_trgm;

-- CREATE INDEX IF NOT EXISTS campaign_gin_idx ON campaign
-- USING GIN ((name || ' ' || product_name || ' ' || brand_name || ' ' || description) gin_trgm_ops);

SELECT 
    t.id
FROM tranche t
INNER JOIN campaign c ON t.campaign_id = c.id
LEFT JOIN tranche_product_categories tpc ON t.id = tpc.tranche_id
LEFT JOIN tranche_social_platforms tsp ON tsp.tranche_id = t.id
WHERE tsp.category_code IN (
  SELECT code FROM social_platform_category
)
AND tpc.category_code IN (
  SELECT code FROM product_category
)
AND c.status = 'PUBLISHED'
AND c.recruitment_end_date >= now()
AND '' <% (c.name || ' ' || c.product_name || ' ' || c.brand_name || ' ' || c.description)
ORDER BY similarity('', (c.name || ' ' || c.product_name || ' ' || c.brand_name || ' ' || c.description)) DESC


-- SELECT k.id, cp.id AS known_client 
-- FROM kol_profile k
-- INNER JOIN public.user u ON k.user_id = u.id
-- INNER JOIN work_contract w ON k.id = w.kol_profile_id
-- INNER JOIN tranche t ON w.tranche_id = t.id
-- INNER JOIN campaign c ON c.id = t.campaign_id
-- INNER JOIN client_profile cp ON cp.id = c.client_profile_id;

-- select * from kol_profile k
-- INNER JOIN user u ON u.id = k.user_id;

-- Step 1: Find kol's social platform
-- Step 2: Query tranches that match filters
-- Step 3: Attach recommended field to each tranche