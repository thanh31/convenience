WITH tranche_with_campaign AS (
    SELECT 
      tranche.id,
      tranche.cash_usd_cents * 0.93 AS kol_pay_amount_usd_cents,
      campaign.id as campaign_id,
      campaign.client_profile_id,
      (campaign.name || ' ' || campaign.product_name || ' ' || campaign.brand_name || ' ' || campaign.description) as campaign_text_combined
    FROM tranche
    INNER JOIN campaign 
        ON tranche.campaign_id = campaign.id
    WHERE campaign.status = 'PUBLISHED' -- Only care about campaign of state PUBLISHED
    AND campaign.recruitment_end_date + interval '24 hours' >= now() -- Buffer 1 day as we want to count on the whole recruitment_end_date
)

SELECT 
    tranche_with_campaign.*,
    tsp.social_platforms,
    tpc.product_categories,
    pc.past_customers
FROM tranche_with_campaign
-- The LEFT JOIN LATERAL allows the subquery (tsp) to reference tranche_with_campaign.id
-- which is the tranche_id from the tranche_with_campaign table in the main query.
-- The ON true condition is used because the LATERAL join's condition is already specified in the WHERE clause of the subquery. 
-- This effectively makes the lateral subquery's result available to the main query's row
LEFT JOIN LATERAL (
    SELECT 
        array_agg(category_code) AS social_platforms
    FROM 
        tranche_social_platforms
    WHERE
        tranche_social_platforms.tranche_id = tranche_with_campaign.id
) tsp ON true
LEFT JOIN LATERAL (
    SELECT 
        array_agg(category_code) AS product_categories
    FROM 
        tranche_product_categories
    WHERE
        tranche_product_categories.tranche_id = tranche_with_campaign.id
) tpc ON true
LEFT JOIN LATERAL (
    -- TODO: When this becomes not performant anymore, can denormalize `client_profile` to store past_customers
    SELECT
        array_agg(distinct work_contract.kol_profile_id) as past_customers
    FROM
        campaign
    INNER JOIN tranche
        ON campaign.id = tranche.campaign_id
    INNER JOIN work_contract
        ON tranche.id = work_contract.tranche_id
    -- Only care about other campaigns of this merchant
    WHERE
        campaign.id <> tranche_with_campaign.campaign_id
        AND campaign.client_profile_id = tranche_with_campaign.client_profile_id
) pc ON true;
