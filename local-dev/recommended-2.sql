-- This is required for fuzzy search
CREATE EXTENSION IF NOT EXISTS pg_trgm; 


-- Input 1: arg_keyword TEXT
-- Input 2: arg_social_codes TEXT[] | default: [tiktok, instagram]
-- Input 3: arg_prev_jobs_applied BOOL | default: false
-- Input 4: arg_wages_from DECIMAL | default: 0
-- Input 5: arg_wages_to DECIMAL | default: 99999999999
-- Input 6: arg_interest_codes TEXT[] | default [...all interest codes...]

-- Index fields in campaign that's used for fuzzy search
-- See more: https://hasura.io/docs/latest/schema/postgres/custom-functions/#example-fuzzy-match-search-functions
CREATE INDEX IF NOT EXISTS campaign_gin_idx ON campaign
USING GIN ((name || ' ' || product_name || ' ' || brand_name || ' ' || description) gin_trgm_ops);

-- This function search tranche internally (not exposed through hasura API)
-- It can be called through hasura action/query
CREATE OR REPLACE FUNCTION search_jobs(
    arg_user_id UUID, 
    arg_keyword TEXT, 
    arg_product_category_codes TEXT[], 
    arg_social_codes TEXT[],
    arg_limit INT, 
    arg_offset INT
)
RETURNS SETOF TABLE (tranche_id UUID, recommended bool)
AS $$
DECLARE
    kol_profile_id UUID;
BEGIN

    -- Potential future use: user_id can be taken from session inside function
    -- session_variables := current_setting('hasura.user', 't');
    -- user_id := (session_variables->>'x-hasura-user-id')::uuid;

    -- Step 0: Find kol profile id
    -- Assuming user has one kol profile for now
    SELECT k.id INTO kol_profile_id FROM kol_profile k
    INNER JOIN public.user u ON k.user_id = u.id
    WHERE u.id = arg_user_id LIMIT 1;

    -- If kol profile not found, throw error
    IF NOT kol_profile_id THEN
        RAISE EXCEPTION 'KOL profile not found';
    END IF;

    -- Step 1: Find kol's social platform codes
    -- Assuming user has one kol profile for now
    -- WITH kol_social_platform_codes AS (
    --     SELECT DISTINCT ksp.social_platform_code FROM kol_profile k
    --     INNER JOIN public.user u ON k.user_id = u.id
    --     INNER JOIN kol_social_platform ksp ON ksp.kol_profile_id = k.id
    --     WHERE u.id = arg_user_id
    -- ),

    -- Step 2.1: Query tranches that match filters
    -- a. campaign status must be in `PUBLISHED` status
    -- b. campaign is still recruiting at this point in time
    -- c. campaign fields match keyword (ordered by similarity from highest to lowest)
    -- d. social platform of tranche belongs to social platform of kol
    WITH filtered_tranche_1 AS (
        SELECT 
            DISTINCT 
                t.id AS tranche_id, 
                c.id AS campaign_id,
                cp.id AS client_profile_id
        FROM tranche t
        INNER JOIN campaign c ON t.campaign_id = c.id
        INNER JOIN tranche_social_platforms tsp ON t.id = tsp.tranche_id
        INNER JOIN client_profile cp ON c.client_profile_id = cp.id
        WHERE tsp.category_code = ANY(arg_social_codes)
        AND c.status = 'PUBLISHED'
        AND c.recruitment_end_date >= now()
        AND arg_keyword <% (c.name || ' ' || c.product_name || ' ' || c.brand_name || ' ' || c.description)
        ORDER BY similarity(arg_keyword, (c.name || ' ' || c.product_name || ' ' || c.brand_name || ' ' || c.description)) DESC
    ),
    -- Step 2.2: Query tranches that match filters
    WITH filtered_tranche_2 AS (
        SELECT
            DISTINCT 
                t.tranche_id, 
                t.campaign_id,
                t.client_profile_id
        FROM filtered_tranche_1 t
        INNER JOIN tranche_product_categories tpc ON t.tranche_id = tpc.tranche_id
        WHERE tpc.category_code = ANY(arg_product_category_codes)
    ),
    -- Step 3.1: Find merchants that kol used to apply in the past
    -- Potential improvement: Store this as field in kol_profile table to avoid re-calculating
    WITH known_clients AS (
        SELECT
            cp.id AS client_profile_id
        FROM kol_profile k
        INNER JOIN public.user u ON k.user_id = u.id
        INNER JOIN work_contract w ON k.id = w.kol_profile_id
        INNER JOIN tranche t ON w.tranche_id = t.id
        INNER JOIN campaign c ON c.id = t.campaign_id
        INNER JOIN client_profile cp ON cp.id = c.client_profile_id
        WHERE u.id = arg_user_id
    ),
    -- Step 3.2: Find kol's interest codes
    WITH kol_interests AS (
        SELECT kppc.interest_code
        FROM kol_profile k
        INNER JOIN public.user u ON k.user_id = u.id
        INNER JOIN kol_profile_product_categories kppc ON k.id = kppc.kol_id
    ),
    -- Step 3: Attach recommended field to each tranche
    WITH filtered_tranche_3 AS (
        SELECT 
            t.tranche_id,
            CASE
                WHEN t.client_profile_id IN (
                    SELECT kc.client_profile_id FROM known_clients kc
                ) AND kpc.category_code IN (
                    SELECT ki.interest_code kol_interests ki
                ) THEN true
                ELSE false
            END AS recommended
        FROM filtered_tranches_2 t
        INNER JOIN tranche_product_categories kpc ON kpc.tranche_id = t.tranche_id
    )
    
    RETURN QUERY SELECT 
        t.tranche_id,
        BOOL_OR(t.recommended) as recommended
    FROM filtered_tranche_3 t
    GROUP BY t.tranche_id;
END;
$$ LANGUAGE plpgsql;

