-- This is required for fuzzy search
CREATE EXTENSION IF NOT EXISTS pg_trgm; 

-- Input 1: [x] arg_keyword TEXT | default: ''
-- Input 2: [x] arg_social_codes TEXT[] | default: [tiktok, instagram]
-- Input 3: [x] arg_prev_jobs_applied BOOL | default: false
-- Input 4: [x] arg_wages_cents_min DECIMAL | default: 0
-- Input 5: [x] arg_wages_cents_max DECIMAL | default: 99999999999
-- Input 6: [x] arg_interest_codes TEXT[] | default [...all interest codes...]
-- Input 7: [x] arg_limit INT | default: 10
-- Input 8: [x] arg_offset INT | default: 0

-- Index fields in campaign that's used for fuzzy search
-- See more: https://hasura.io/docs/latest/schema/postgres/custom-functions/#example-fuzzy-match-search-functions
CREATE INDEX IF NOT EXISTS campaign_gin_idx ON campaign
USING GIN ((name || ' ' || product_name || ' ' || brand_name || ' ' || description) gin_trgm_ops);

CREATE TYPE search_jobs_result AS (
    campaign_id UUID,
    tranche_ids UUID[],
    recommended bool
);


-- This function search tranche internally (not exposed through hasura API)
-- It can be called through hasura action/query
CREATE OR REPLACE FUNCTION search_jobs(
    arg_user_id UUID, 
    arg_keyword TEXT, 
    arg_social_codes TEXT[], 
    arg_prev_jobs_applied BOOLEAN,
    arg_wages_cents_min DECIMAL,
    arg_wages_cents_max DECIMAL,
    arg_interest_codes TEXT[],
    arg_limit INT, 
    arg_offset INT
)
RETURNS SETOF search_jobs_result
AS $$
DECLARE
    kol_profile_id UUID;
    past_client_ids UUID[];
    kol_interest_codes TEXT[];
BEGIN

    -- Potential future use: user_id can be taken from session inside function
    -- session_variables := current_setting('hasura.user', 't');
    -- user_id := (session_variables->>'x-hasura-user-id')::uuid;

    -- Step 1: Find kol profile id
    -- Assuming user has one kol profile for now
    SELECT k.id INTO kol_profile_id FROM kol_profile k
    INNER JOIN public.user u ON k.user_id = u.id
    WHERE u.id = arg_user_id LIMIT 1;

    -- If kol profile not found, throw error
    IF NOT kol_profile_id THEN
        RAISE EXCEPTION 'KOL profile not found';
    END IF;

    -- Step 2: Find prev merchants that kol used to apply (has work contract) in the past
    SELECT ARRAY_AGG(DISTINCT c.client_profile_id) INTO past_client_ids 
    FROM work_contract w
    INNER JOIN tranche t ON w.tranche_id = t.id
    INNER JOIN campaign c ON t.campaign_id = c.id
    WHERE w.kol_profile_id = kol_profile_id;

    -- Step 3: Find kol's interest codes
    SELECT kpc.interest_code INTO kol_interest_codes
    FROM kol_profile k
    INNER JOIN kol_profile_product_categories kpc ON k.id = kpc.kol_id;

    -- Step 4: Query tranches that match filters
    -- a. campaign status must be in `PUBLISHED` status
    -- b. campaign is still recruiting at this point in time (use recruitment_end_date)
    -- c. campaign fields match keyword (ordered by similarity from highest to lowest)
    -- d. social platform of tranche belongs to arg_social_codes
   	RETURN QUERY 
    WITH filtered_tranche_1 AS (
        SELECT 
            DISTINCT 
                t.id AS tranche_id, 
                c.id AS campaign_id,
                c.created_at AS campaign_created_at,
                cp.id AS client_profile_id
        FROM tranche t
        INNER JOIN campaign c ON t.campaign_id = c.id
        INNER JOIN tranche_social_platforms tsp ON t.id = tsp.tranche_id
        INNER JOIN client_profile cp ON c.client_profile_id = cp.id
        WHERE tsp.category_code IN (SELECT unnest(arg_social_codes))
        -- (cash_usd_cents * 0.93) is the amount that kol will receive
        AND c.cash_usd_cents * 0.93 >= arg_wages_cents_min AND c.cash_usd_cents * 0.93 <= arg_wages_cents_max
        AND c.status = 'PUBLISHED'
        AND c.recruitment_end_date >= now()
        AND CASE WHEN arg_prev_jobs_applied THEN c.client_profile_id IN (SELECT unnest(past_client_ids)) ELSE true END
        AND arg_keyword <% (c.name || ' ' || c.product_name || ' ' || c.brand_name || ' ' || c.description)
        ORDER BY similarity(arg_keyword, (c.name || ' ' || c.product_name || ' ' || c.brand_name || ' ' || c.description)) DESC
    ),

    -- Step 5: Attach recommended field to each tranche
    filtered_tranche_2 AS (
        SELECT
            DISTINCT
                t.tranche_id,
                t.campaign_id,
                t.campaign_created_at,
                CASE
                    WHEN t.client_profile_id IN (SELECT unnest(past_client_ids)) 
                        AND kpc.category_code IN (SELECT unnest(kol_interest_codes)) 
                    THEN true
                    ELSE false
                END AS recommended
        FROM filtered_tranche_1 t
        INNER JOIN tranche_product_categories kpc ON kpc.tranche_id = t.tranche_id
    )

    -- Goal for next query:

    -- tranche_id | campaign_id | category_code | recommended
    -----------------------------------------
    -- 1,           1               tiktok,         false
    -- 2,           1               instagram,      true
    -- 3,           1               facebook,       false
    -- 4,           2               facebook,       false
    -- 5,           2               tiktok,         false
    -- ========================================

    -- campaign_id | tranche_ids | recommended
    -----------------------------------------
    -- 1,           [2, 1, 3],      true
    -- 2,           [4, 5],         false
    
    SELECT 
        t.campaign_id,
        ARRAY_AGG(t.tranche_id) AS tranche_ids,
        BOOL_OR(t.recommended) as recommended
    FROM filtered_tranche_2 t
    GROUP BY t.campaign_id
    ORDER BY BOOL_OR(t.recommended) DESC, t.campaign_created_at DESC
    LIMIT arg_limit OFFSET arg_offset;
END;
$$ LANGUAGE plpgsql;

